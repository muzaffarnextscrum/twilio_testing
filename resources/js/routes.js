import VueRouter from "vue-router";
import VideoChat from './components/VideoChat';
import testVideo from './components/testVideo';

const routes = [{
        path: "/",
        component: VideoChat,
        name: "homes",
    },
    {
        path: "/video",
        component: VideoChat,
        name: "video",
    },
    {
        path: "/test1",
        component: testVideo,
        name: "video1",
    },
];

const router = new VueRouter({
    routes, // short for `routes: routes`
    mode: "history"

});

export default router;