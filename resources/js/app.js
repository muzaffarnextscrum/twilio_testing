
require('./bootstrap');
import VueRouter from "vue-router";
import router from "./routes";
window.Vue = require('vue');
import Index from './index';

Vue.use(VueRouter);
Vue.component('video-chat', require('./components/VideoChat.vue').default);

// console.log(store.state.count)
// const storeVal = new Vuex.Store(storeDefination);

const app = new Vue({
    el: "#app",
    router,
    components: {
        'index': Index
    },

});
