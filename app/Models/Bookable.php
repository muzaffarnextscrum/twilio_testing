<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bookable extends Model
{
  protected  $table = 'bookables';
  use HasFactory;
  protected $fillable = [
    'title', 'description'
  ];
  public function bookings()
  {
    return $this->hasMany(bookings::class);
  }
  public function review()
    {
        return $this->hasOne(Reviews::class);
    }
}
