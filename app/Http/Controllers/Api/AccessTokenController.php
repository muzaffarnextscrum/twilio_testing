<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;

class AccessTokenController extends Controller
{
    //

    public function generate_token()
    {
        // Substitute your Twilio Account SID and API Key details
       
        // $accountSid = env('TWILIO_ACCOUNT_SID');
        // $apiKeySid = env('TWILIO_API_KEY_SID');
        // $apiKeySecret = env('TWILIO_API_KEY_SECRET');
      
         $accountSid = 'AC176844e4d93625bc5ee3dc9e80741233';
        $apiKeySid = 'SK5d0d474f4bb2c7a3e83e9b63f04f457c';
        $apiKeySecret = '8Otdnd155g7wKkc5SlRhFGim5pnRn0wy';
        $identity = uniqid();

        // Create an Access Token
        $token = new AccessToken(
            $accountSid,
            $apiKeySid,
            $apiKeySecret,
            3600,
            $identity
        );

        // Grant access to Video
        $grant = new VideoGrant();
        $grant->setRoom('cool room');
        $token->addGrant($grant);

        // Serialize the token as a JWT
        echo $token->toJWT();
    }
}
